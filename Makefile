
SLIDES= title old both

SLIDEFILES=$(addsuffix .ps, $(SLIDES))

o= >$@.new && mv -f $@.new $@

all:	slides.pdf

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 <$@.1 $o

%.ps:	%.lout
	lout $< $o

both.fig: old.fig new.fig
	cat old.fig >$@.1
	sed '1,9d' new.fig >>$@.1
	mv -f $@.1 $@

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

slides.pdf:     slides.ps Makefile
	ps2pdf $< $@

install: slides.pdf gobby.txt
	rsync -vP $^ ijackson@chiark:public-html/2014/debconf-builds-bof/
	git push ijackson@chiark:public-git/talk-2014-debconf-builds-bof.git
