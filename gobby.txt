
Rejected options:
  ia32-libs style single giant script that builds things in
  the right order (somehow getting right source packages)
  
  Partial architecture cross-build (would need several of
  these and build would be complicated and patches to
  upstream/debian source for the weird environment would
  be hard)

Better:
  python-rumpxen_amd64.deb
  built from python-rumpxen.dsc
  where does it get actual python source?

four possibilities for that:
  add a _copy_ of upstream python source (urgh)
  add _source.deb for all relevant packages (tiresome)
  source dependencies (needs lots of work everywhere)
  run apt-get source in debian/rules (urgh but there is precedent)

Precedent for apt-get source:
  debian-installer
    fetches udebs from archive from debian/rules
  android.dsc in Ubuntu
    fetches sources from archive

Want some automation to get build-using right, etc.
Go with that.